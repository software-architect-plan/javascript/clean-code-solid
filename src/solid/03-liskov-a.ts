import { Tesla, Audi, Toyota, Honda, Volvo, Ford, Vehicule } from './03-liskov-b';


(() => {
    
    const printCarSeats = ( cars: Vehicule[] ) => {
        
        cars.forEach( car => {
            const brandVehicule = car.constructor.name
            console.log( brandVehicule, car.getNumberOfSeats())
        })

        // for (const car of cars) {
         
        //     if( car instanceof Tesla ) {
        //         console.log( 'Tesla', car.getNumberOfSeats() )
        //         continue;
        //     }
        //     if( car instanceof Audi ) {
        //         console.log( 'Audi', car.getNumberOfSeats() )
        //         continue;
        //     }
        //     if( car instanceof Toyota ) {
        //         console.log( 'Toyota', car.getNumberOfSeats() )
        //         continue;
        //     }
        //     if( car instanceof Honda ) {
        //         console.log( 'Honda', car.getNumberOfSeats() )
        //         continue;
        //     }         
        //     if( car instanceof Volvo ) {
        //         console.log( 'Honda', car.getNumberOfSeats() )
        //         continue;
        //     }
        // }
    }
    
    const cars = [
        new Tesla(7),
        new Audi(2),
        new Toyota(5),
        new Honda(5),
        new Volvo(3),
        new Ford(2)
    ];


    printCarSeats( cars );

})();