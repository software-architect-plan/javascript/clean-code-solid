interface Bird {
    eat(): void;
}

interface FlyingBird {
    fly(): number;
}

interface RunningBird {
    run(): void;
}

interface SwimerBird {
    swim(): void;
}

class Tucan  implements Bird, FlyingBird{
    public run(): void {}
    public fly(): number {
        return 30;
    }
    public eat() {}
    public swim() {}
}

class HumminBird implements Bird, FlyingBird{
    public fly(): number {
        return 110
    }
    public eat() {}
    public run() {}
    public swim() {}
}

class Ostrich implements Bird, RunningBird {
    public eat(): void {}
    public run(): void {}
}

class Penguin implements Bird, SwimerBird {
    public eat(): void {}
    public swim() {}
    
}