(() => {

    // No aplicando el principio de responsabilidad única

    type Gender = 'M'|'F'

    class Person {
        constructor(public name: string,
            public gender: Gender,
            public birthdate: Date) {
            
        }
    }

    // const newPerson = new Person('Felipe', 'M', new Date('1992-10-28'))
    // console.log({ newPerson })

    class User extends Person {
        
        public lastAccess: Date;

        constructor(
            public email: string,
            public role: string,
            name: string,
            gender: Gender,
            birthdate: Date
        ) {
            super(name, gender, birthdate);
            this.lastAccess = new Date();
        }

        checkCredentials() {
            return true
        }
    }

    class UserSettings extends User {
        constructor(
            public workingDirectory: string,
            public lastOpenFolder: string,
            email: string,
            role: string,
            name: string,
            gender: Gender,
            birthdate: Date
        ){
            super(email, role, name, gender, birthdate);
        }
    }


    const userSetting = new UserSettings(
        '/usr/home',
        '/home',
        'felipe@gmail.com',
        'Admin',
        'Felipe',
        'M',
        new Date('1992-10-28')
    )

    console.log({ userSetting })

    

})()